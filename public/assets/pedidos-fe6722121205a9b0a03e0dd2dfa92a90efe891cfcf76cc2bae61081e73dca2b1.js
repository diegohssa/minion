(function() {
  $('.changeColor').on('click', function(event) {
    var cor, id;
    id = this.id;
    cor = id.replace("filtro", "");
    $('#imgPrincipal').removeClass('filtroAmarelo filtroVerde filtroAzul');
    $('#imgPrincipal').addClass(id);
    return $('#cor').val(cor);
  });

}).call(this);
