class CreatePedidos < ActiveRecord::Migration[5.1]
  def change
    create_table :pedidos do |t|
      t.string :nome
      t.string :email
      t.string :cor
      t.integer :quantidade

      t.timestamps
    end
  end
end
