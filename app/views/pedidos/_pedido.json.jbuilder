json.extract! pedido, :id, :nome, :email, :cor, :quantidade, :created_at, :updated_at
json.url pedido_url(pedido, format: :json)
