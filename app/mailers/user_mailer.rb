class UserMailer < ApplicationMailer
  default from: 'diegoh@gmail.com'
 
  def welcome_email
    @user = params[:user]
    @url  = 'http://example.com/login'
    mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  end    
  
  def welcome_email(pedido)
    @pedido = pedido
    @url = 'https://boiling-wave-55815.herokuapp.com/'
    mail(to: @pedido.email, subject: 'Seu minion foi reservado com sucesso')
  end  
    
end