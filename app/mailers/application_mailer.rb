class ApplicationMailer < ActionMailer::Base
  default from: 'diegoh@gmail.com'
  layout 'mailer'
end
