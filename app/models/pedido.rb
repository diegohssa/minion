class Pedido < ApplicationRecord
    validates :nome, presence: true
    validates :email, presence: true    
    validates :cor, presence: true    
    validates :quantidade, presence: true     
end
